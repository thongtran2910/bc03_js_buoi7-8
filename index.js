var numberArr = [];
var soThucArr = [];

function kiemTraSnt(n) {
  var laSoNT = 1;
  if (n < 2) {
    return (laSoNT = 0);
  }
  var item = 2;
  while (item < n) {
    if (n % item == 0) {
      laSoNT = 0;
      break;
    }
    item++;
  }
  return laSoNT;
}
// xuất mảng
function xuatMang() {
  var soNguyen = document.getElementById("txt-so-nguyen").value * 1;
  numberArr.push(soNguyen);
  // console.log(numberArr);
  document.getElementById("ketQua").innerHTML = `<p>Mảng: ${numberArr}</p>`;
}
// tổng số dương
function tinhTong() {
  var soDuongArr = [];
  var tongSoDuong = 0;
  numberArr.forEach(function (item) {
    if (item >= 0) {
      soDuongArr.push(item);
    }
  });
  soDuongArr.forEach(function (item) {
    tongSoDuong += item;
  });
  document.getElementById(
    "tongSoDuong"
  ).innerHTML = `<p>Tổng số dương: ${tongSoDuong}</p>`;
}
// đếm số dương
function demSo() {
  var soDuongArr = [];
  numberArr.forEach(function (item) {
    if (item >= 0) {
      soDuongArr.push(item);
    }
  });
  document.getElementById(
    "soLuongSoDuong"
  ).innerHTML = `<p>Số lượng số dương trong mảng: ${soDuongArr.length}</p>`;
}
// tìm số nhỏ nhất
function timSoMin() {
  var soNhoNhat = numberArr[0];
  numberArr.forEach(function (item) {
    if (item < soNhoNhat) {
      soNhoNhat = item;
    }
  });
  document.getElementById(
    "soNhoNhat"
  ).innerHTML = `<p>Số nhỏ nhất trong mảng: ${soNhoNhat}</p>`;
}
// tìm số dương nhỏ nhất
function timSoDuongMin() {
  var soDuongNhoNhat = numberArr[0];
  numberArr.forEach(function (item) {
    if (soDuongNhoNhat > item && item > 0) {
      soDuongNhoNhat = item;
    }
  });
  document.getElementById(
    "soDuongNhoNhat"
  ).innerHTML = `<p>Số dương nhỏ nhất trong mảng: ${soDuongNhoNhat}</p>`;
}
// tìm số chẵn
function timSoChan() {
  var soChanArr = [];
  var soChanCuoiCung = -1;
  numberArr.forEach(function (item) {
    if (item % 2 === 0) {
      soChanArr.push(item);
      soChanCuoiCung = soChanArr[soChanArr.length - 1];
    }
    // console.log("soChanArr", soChanArr);
  });
  document.getElementById(
    "soChan"
  ).innerHTML = `<p>Số chẵn cuối cùng trong mảng: ${soChanCuoiCung}</p>`;
}
// đổi vị trí
function doiCho() {
  var viTri1 = document.getElementById("txt-vitri-1").value * 1;
  var viTri2 = document.getElementById("txt-vitri-2").value * 1;

  var temp = numberArr[viTri1];
  numberArr[viTri1] = numberArr[viTri2];
  numberArr[viTri2] = temp;

  document.getElementById("doiViTri").innerHTML =
    "Mảng sau khi đổi vị trí: " + numberArr;
  // console.log("numberArr", numberArr);
}
// sắp xếp mảng theo thứ tự tăng dần
function sapXep() {
  numberArr.sort(function (a, b) {
    return a - b;
  });
  document.getElementById("sapXep").innerHTML =
    "Sắp xếp mảng theo thứ tự tăng dần: " + numberArr;
}
//  tìm số nguyên tố đầu tiên
function timSoNT() {
  var soNguyenToDauTien = -1;
  for (var i = 0; i < numberArr.length; i++) {
    if (kiemTraSnt(numberArr[i]) == 1) {
      soNguyenToDauTien = numberArr[i];
      break;
    }
  }
  document.getElementById(
    "soNtDauTien"
  ).innerHTML = `<p>Số nguyên tố đầu tiên trong mảng: ${soNguyenToDauTien}</p>`;
}
// xuất mảng số thực và đếm số nguyên trong mảng
function xuatMangSoThuc() {
  var soThuc = document.getElementById("txt-so-thuc").value * 1;
  soThucArr.push(soThuc);
  document.getElementById("soThuc").innerHTML = `<p>Mảng: ${soThucArr}`;
}
function demSoNguyen() {
  soNguyenArr = [];
  for (var i = 0; i < soThucArr.length; i++) {
    if (Number.isInteger(soThucArr[i]) == true) {
      soNguyenArr.push(i);
    }
  }
  document.getElementById(
    "soLuongSoNguyen"
  ).innerHTML = `<p>Số lượng số nguyên trong mảng số thực: ${soNguyenArr.length}`;
}
// so sánh số lượng số âm và số dương
function soSanh() {
  var soAmArr = [];
  var soDuongArr = [];
  numberArr.forEach(function (item) {
    if (item >= 0) {
      soDuongArr.push(item);
    } else {
      soAmArr.push(item);
    }
  });
  if (soDuongArr.length > soAmArr.length) {
    document.getElementById(
      "soSanhSL"
    ).innerHTML = `<p>Số lượng số dương nhiều hơn số âm</p>`;
  } else if (soDuongArr.length < soAmArr.length) {
    document.getElementById(
      "soSanhSL"
    ).innerHTML = `<p>Số lượng số âm nhiều hơn số dương</p>`;
  } else {
    document.getElementById(
      "soSanhSL"
    ).innerHTML = `<p>Số lượng số dương và số lượng số âm bằng nhau</p>`;
  }
}
